<?php

declare(strict_types = 1);

namespace Drupal\error_page;

/**
 * Provides an interface for PHP error logger.
 */
interface ErrorPagePhpErrorLoggerInterface {

  /**
   * Send an error message to the PHP log.
   *
   * @param string $log_entry
   *   The message to be logged.
   */
  public static function log(string $log_entry): void;

}
