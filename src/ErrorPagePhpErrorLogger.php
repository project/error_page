<?php

declare(strict_types = 1);

namespace Drupal\error_page;

use Drupal\Core\Site\Settings;

/**
 * Wraps the native PHP error_log() function.
 *
 * @see error_log()
 */
class ErrorPagePhpErrorLogger implements ErrorPagePhpErrorLoggerInterface {

  /**
   * {@inheritdoc}
   */
  public static function log(string $log_entry): void {
    $settings = Settings::get('error_page');
    // @see https://www.php.net/manual/en/function.error-log.php
    $log_method = $settings['log']['method'] ?? 0;
    if (isset($settings['log']['destination'])) {
      error_log($log_entry, $log_method, $settings['log']['destination']);
    }
    else {
      error_log($log_entry, $log_method);
    }
  }

}
